package ru.leondopulus.mineSweeper.business;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.leondopulus.mineSweeper.GameException;
import ru.leondopulus.mineSweeper.data.GameState;
import ru.leondopulus.mineSweeper.data.IGameRepository;

import java.util.UUID;

@Service
public class GameService {

    private final IGameRepository repository;

    public GameService(@Qualifier("InMemory") IGameRepository repository) {

        this.repository = repository;
    }

    public String createGame(int width, int height, int minesCount) throws GameException {
        if (width > 30) throw new GameException("ширина поля должна быть не менее 2 и не более 30");
        if (height > 30 ) throw new GameException("высота поля должна быть не менее 2 и не более 30");
        if (minesCount > width * height - 1) throw new GameException("количество мин должно быть не менее 1 и не более " + (width * height -1));

        //создать поле
        int[][] field = new int[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                field[i][j] = 0;
            }
        }

        //зарандомить мины
        int minesLeft = minesCount;
        while (minesLeft > 0) {
            int row = (int) (Math.random() * width);
            int col = (int) (Math.random() * height);
            if (field[row][col] == 0) {
                field[row][col] = 9;
                minesLeft--;
            }
        }

        //заполнить остальные поля
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (field[i][j] == 9) {

                    for (int k = i - 1; k <= i + 1; k++) {
                        if (k < 0 || k >= width) continue;
                        for (int l = j - 1; l <= j + 1; l++) {
                            if (l < 0 || l >= height) continue;

                            if (field[k][l] < 9) {
                                field[k][l]++;
                            }

                        }
                    }

                }
            }
        }

        //сгенерировать id
        String id = UUID.randomUUID().toString();

        //создать сессию
        GameState newState = new GameState()
                .setGameId(id)
                .setWidth(width)
                .setHeight(height)
                .setMinesCount(minesCount)
                .setCompleted(false)
                .setField(field);

        setGameState(newState);

        return id;
    }

    public void makeGameTurn(String id, int row, int col) throws GameException {

        GameState state = getGameState(id);

        state.checkCell(row, col);

        setGameState(state);
    }

    public GameState getGameState(String id) {

        return repository.getSession(id);
    }

    private void setGameState(GameState state) {

        repository.putSession(state);
    }


}
