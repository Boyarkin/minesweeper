package ru.leondopulus.mineSweeper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

@SpringBootApplication
public class MineSweeperApplication {

	public static void main(String[] args) {

		SpringApplication.run(MineSweeperApplication.class, args);
	}

	@Bean
	MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		return new MappingJackson2HttpMessageConverter();
	}

}
