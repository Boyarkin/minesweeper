package ru.leondopulus.mineSweeper.api.responses;

public class ErrorResponse {
    private String error;

    public ErrorResponse(String errorMessage) {
        error = errorMessage;
    }

    public String getError() {
        return error;
    }

}
