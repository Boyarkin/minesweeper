package ru.leondopulus.mineSweeper.api.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GameTurnRequest {

    @JsonProperty("game_id")
    private String gameId;
    private int col;
    private int row;

    public String getGameId() {
        return gameId;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

}
