package ru.leondopulus.mineSweeper.api.responses;


import com.fasterxml.jackson.annotation.JsonProperty;

public class GameInfoResponse {

    @JsonProperty("game_id")
    private String gameId;
    private int width;
    private int height;
    @JsonProperty("mines_count")
    private int minesCount;
    private Boolean completed = false;
    String[][] field;

    //------------------------------------------------
    public String getGameId() {
        return gameId;
    }

    public GameInfoResponse setGameId(String gameId) {
        this.gameId = gameId;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public GameInfoResponse setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public GameInfoResponse setHeight(int height) {
        this.height = height;
        return this;
    }

    public int getMinesCount() {
        return minesCount;
    }

    public GameInfoResponse setMinesCount(int minesCount) {
        this.minesCount = minesCount;
        return this;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public GameInfoResponse setCompleted(Boolean completed) {
        this.completed = completed;
        return this;
    }

    public String[][] getField() {
        return field;
    }

    public GameInfoResponse setField(int[][] field) {

        this.field = new String[field.length][field[0].length];
        for (int i = 0; i < field.length; i++){
            for (int j = 0; j < field[i].length; j++){

                int cell = field[i][j];
                String newCell;

                if (cell < 100){
                    newCell = " ";
                }else {
                    newCell = switch (cell) {
                        case 110 -> "M";
                        case 109 -> "X";
                        default -> String.valueOf(cell - 100);
                    };
                }

                this.field[i][j] = newCell;
            }
        }
        return this;
    }
}
