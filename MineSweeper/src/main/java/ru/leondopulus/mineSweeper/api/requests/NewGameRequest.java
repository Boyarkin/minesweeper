package ru.leondopulus.mineSweeper.api.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewGameRequest {

    private int width = 10;
    private int height = 10;
    @JsonProperty("mines_count")
    private int minesCount;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getMinesCount() {
        return minesCount;
    }

}