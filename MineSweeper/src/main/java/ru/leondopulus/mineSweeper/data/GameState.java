package ru.leondopulus.mineSweeper.data;

import ru.leondopulus.mineSweeper.GameException;

public class GameState {
    private String gameId;
    private int width;
    private int height;
    private int minesCount;
    private Boolean completed = false;
    private int[][] field;

    public void checkCell(int row, int col) throws GameException {
        if (completed == true) throw new GameException("игра завершена");
        if (field[row][col] >= 100) throw new GameException("уже открытая ячейка");
        open( row,  col);
    }

    private void open(int row, int col) {
         if (!isInBounds(row, col)) return;

        if (field[row][col] == 9) {openAll(false); return;}

        if (field[row][col] == 0){

            field[row][col] += 100;

            //с +0 проще читать
            open(row -1, col -1);
            open(row +0, col -1);
            open(row +1, col -1);

            open(row -1, col );
            open(row +1, col );

            open(row -1, col +1);
            open(row +0, col +1);
            open(row +1, col +1);

        }else if(field[row][col] < 10){
            field[row][col] += 100;
            checkEnd();
        }
    }

    public void openAll(Boolean isWin) {

        int mine = isWin ? 110 : 109;

        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {

                if (field[i][j] == 9) {
                    field[i][j] = mine;

                } else if (field[i][j] < 100) {
                    field[i][j] += 100;
                }
            }
        }

        completed = true;
    }

    private void checkEnd(){
        //если кроме мин ничего не осталось - выигрыш
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                if (field[i][j] < 9) return;
            }
        }
        openAll(true);
    }

    private Boolean isInBounds(int row, int col){
        return row >= 0 && row < field.length && col >= 0 && col < field[0].length;
    }

    //---------Getters-and-Setters-------------------------
    public String getGameId() {
        return gameId;
    }

    public GameState setGameId(String gameId) {
        this.gameId = gameId;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public GameState setWidth(int width) {
        this.width = width;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public GameState setHeight(int height) {
        this.height = height;
        return this;
    }

    public int getMinesCount() {
        return minesCount;
    }

    public GameState setMinesCount(int minesCount) {
        this.minesCount = minesCount;
        return this;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public GameState setCompleted(Boolean completed) {
        this.completed = completed;
        return this;
    }

    public int[][] getField() {
        return field;
    }

    public GameState setField(int[][] field) {
        this.field = field;
        return this;
    }

}
