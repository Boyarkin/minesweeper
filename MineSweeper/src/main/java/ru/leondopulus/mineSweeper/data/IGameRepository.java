package ru.leondopulus.mineSweeper.data;

public interface IGameRepository {


    void putSession(GameState gameState);

    GameState getSession(String id);
}
