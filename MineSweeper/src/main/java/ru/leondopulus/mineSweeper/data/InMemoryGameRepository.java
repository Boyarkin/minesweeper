package ru.leondopulus.mineSweeper.data;

import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository(value = "InMemory")
public class InMemoryGameRepository implements IGameRepository {

    private HashMap<String, GameState> allSessions = new HashMap<>();

    @Override
    public void putSession(GameState gameState){

        allSessions.put(gameState.getGameId(), gameState);
    }

    @Override
    public GameState getSession(String id){
        return allSessions.get(id);
    }

}
