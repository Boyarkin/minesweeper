package ru.leondopulus.mineSweeper;

import org.springframework.web.bind.annotation.*;
import ru.leondopulus.mineSweeper.api.requests.GameTurnRequest;
import ru.leondopulus.mineSweeper.api.requests.NewGameRequest;
import ru.leondopulus.mineSweeper.api.responses.GameInfoResponse;
import ru.leondopulus.mineSweeper.business.GameService;
import ru.leondopulus.mineSweeper.data.GameState;


@RestController
public class GameController {

    private GameService service;

    public GameController(GameService service) {
        this.service = service;
    }

    @PostMapping(value = "/new")
    public GameInfoResponse newGameRequest(@RequestBody NewGameRequest request) throws GameException {

        String id = service.createGame(
                request.getWidth(),
                request.getHeight(),
                request.getMinesCount());

        return getInfoResponse(id);
    }

    @PostMapping(value = "/turn")
    public GameInfoResponse gameTurnRequest(@RequestBody GameTurnRequest request) throws GameException {

        service.makeGameTurn(
                request.getGameId(),
                request.getRow(),
                request.getCol());

        return getInfoResponse(request.getGameId());
    }

    private GameInfoResponse getInfoResponse(String id){

        GameState state = service.getGameState(id);
        GameInfoResponse response = new GameInfoResponse()
                .setGameId(state.getGameId())
                .setWidth(state.getWidth())
                .setHeight(state.getHeight())
                .setMinesCount(state.getMinesCount())
                .setCompleted(state.getCompleted())
                .setField(state.getField());

        return response;
    }

}
